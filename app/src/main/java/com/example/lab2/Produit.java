package com.example.lab2;

import java.util.ArrayList;

public class Produit {
public static ArrayList<Produit> cart= new ArrayList<>();

    private String nom;
    private String img;
    private String prix;
    private String info;
    public int qty=0;
    double price;


    public Produit(String nom,String img,String info,String prix)
    {
        this.nom=nom;
        this.img=img;
        this.info=info;
        this.prix=prix;
        if (prix.contains("/")){
            String[] arrOfStr = prix.split("/", 3);
            String a = arrOfStr[0];
            String b = arrOfStr[1];

            int i= b.length()-1;
            String c = b.substring(2, i+1);
            //System.out.println(c);
            price = Double.valueOf(c)/Double.valueOf(a);
        }
        else{
            String[] arrOfStr = prix.split(" ", 3);
            String a = arrOfStr[0];
            String b="";
            b=a;
            int i= b.length()-1;
            b = a.substring(1, i+1);
            price = Double.valueOf(b);
        }
    }
    public Produit(String nom,String img,String info,String prix,int qty,double price)
    {
        this.nom=nom;
        this.img=img;
        this.info=info;
        this.prix=prix;
        this.qty=qty;
        this.price = price;

    }

    public String getNom()
    {
        return nom;
    }
    public String getImg()
    {
        return img;
    }
    public String getInfo()
    {
        return info;
    }
    public String getPrix()
    {
        return prix;
    }
    public double getPrice()
    {
        return price;
    }
}
