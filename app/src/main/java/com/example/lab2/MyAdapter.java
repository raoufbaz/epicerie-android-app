package com.example.lab2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends ArrayAdapter<Produit> {
    private Context c;
    ArrayList<Produit> productList = new ArrayList<>();

    public MyAdapter(Context context, int textViewResourceId, ArrayList<Produit> objects) {
        super(context, textViewResourceId, objects);
        productList = objects;
        this.c = c;

    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.grid_view_items, null);
        final TextView textView = (TextView) v.findViewById(R.id.textView);
        final TextView textinfo = (TextView) v.findViewById(R.id.info);
        final ImageView imageView = (ImageView) v.findViewById(R.id.imageView);
         final TextView qty = (TextView) v.findViewById(R.id.qty);
        Button plus = (Button)v.findViewById(R.id.buttonplus);
        Button moins = (Button)v.findViewById(R.id.buttonminus);
        Button add = (Button)v.findViewById(R.id.button);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = Integer.valueOf(qty.getText().toString());
                i++;
                qty.setText(String.valueOf(i));
            }
        });
        moins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = Integer.valueOf(qty.getText().toString());
                if (i>0)
                    i--;
                qty.setText(String.valueOf(i));
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean b=false;
                if (qty.getText().toString().contains("0")){
                    Toast.makeText(v.getContext(), "Add a quantity", Toast.LENGTH_SHORT).show();
                }
                else{
                for (int i=0;i<Produit.cart.size();i++){
                    if (Produit.cart.get(i).getNom()==textView.getText().toString()){
                        Produit.cart.get(i).qty+=Integer.valueOf(qty.getText().toString());
                        b=true;
                    }
                }
                if(b==false)
               Produit.cart.add(new Produit(textView.getText().toString(),imageView.getTag().toString(),textinfo.getText().toString(),textinfo.getText().toString(),Integer.valueOf(qty.getText().toString()),Double.valueOf(textinfo.getTag().toString())));

                Toast.makeText(v.getContext(), qty.getText().toString() +" "+ textView.getText().toString()+" added to cart", Toast.LENGTH_SHORT).show();
                //Toast.makeText(v.getContext(), imageView.getTag().toString(), Toast.LENGTH_SHORT).show();
            }
            }
        });
        textView.setText(productList.get(position).getNom());
        textinfo.setText(productList.get(position).getPrix());
        Picasso.get().load(productList.get(position).getImg()).into(imageView);
        imageView.setTag(productList.get(position).getImg());
        textinfo.setTag(productList.get(position).getPrice());

        return v;
    }

}