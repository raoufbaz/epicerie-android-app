package com.example.lab2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CartAdapter  extends ArrayAdapter<Produit> {
    private Context c;
    ArrayList<Produit> productList = new ArrayList<>();

    public CartAdapter(Context context, int textViewResourceId, ArrayList<Produit> objects) {
        super(context, textViewResourceId, objects);
        productList = objects;
        this.c = c;

    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.grid_view_cartitems, null);
        final TextView textView = (TextView) v.findViewById(R.id.textView);
        final TextView textinfo = (TextView) v.findViewById(R.id.info);
        final ImageView imageView = (ImageView) v.findViewById(R.id.imageView);
        final TextView qty = (TextView) v.findViewById(R.id.qty);
        Button plus = (Button)v.findViewById(R.id.buttonplus);
        Button moins = (Button)v.findViewById(R.id.buttonminus);
        Button add = (Button)v.findViewById(R.id.button);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = Integer.valueOf(qty.getText().toString());
                i++;
                qty.setText(String.valueOf(i));
            }
        });
        moins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = Integer.valueOf(qty.getText().toString());
                if (i>0)
                    i--;
                qty.setText(String.valueOf(i));
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(), textView.getText().toString() +" "+" removed from cart", Toast.LENGTH_SHORT).show();
                Toast.makeText(v.getContext(), textView.getText().toString() +" "+" removed from cart", Toast.LENGTH_SHORT).show();
                for (int i=0;i<Produit.cart.size();i++) {
                    if (Produit.cart.get(i).getNom() == textView.getText().toString()){
                        Produit.cart.remove(i);
                        i++;
                    }
                    i++;
                }

                Intent Facteur = new Intent(v.getContext(),PanierActivity.class);
                v.getContext().startActivity(Facteur);
                ((Activity)v.getContext()).finish();
            }
        });
        textView.setText(productList.get(position).getNom());
        textinfo.setText(productList.get(position).getPrix());
        qty.setText(String.valueOf(productList.get(position).qty));
        Picasso.get().load(productList.get(position).getImg()).into(imageView);
        return v;
    }

}