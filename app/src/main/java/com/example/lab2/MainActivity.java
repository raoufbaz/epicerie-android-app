package com.example.lab2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    GridView simpleList;
    ArrayList<Produit> productList=new ArrayList<>();
    TextView qty;
    TextView nom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Fruits & Vegetables");
        qty = (TextView)findViewById(R.id.qty);
        nom = (TextView)findViewById(R.id.textView);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Facteur = new Intent(view.getContext(),PanierActivity.class);
                //Facteur.putExtra("mesage",msg.getText().toString());
                startActivity(Facteur);
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                  //      .setAction("Action", null).show();
            }
        });

         new bgWorker().execute("https://www.metro.ca/en/online-grocery/aisles/fruits-vegetables");
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_dairy) {
//            getSupportActionBar().setTitle("Dairy & Eggs");
//            productList.clear();
//            new bgWorker().execute("https://www.metro.ca/en/online-grocery/aisles/dairy-eggs");
//            return true;
//        }
        switch(id) {
            case R.id.action_fruit:
                productList.clear();
                getSupportActionBar().setTitle("Fruits & Vegetables");
                new bgWorker().execute("https://www.metro.ca/en/online-grocery/aisles/fruits-vegetables");
                return true;
            case R.id.action_dairy:
                productList.clear();
                getSupportActionBar().setTitle("Dairy & Eggs");
                new bgWorker().execute("https://www.metro.ca/en/online-grocery/aisles/dairy-eggs");
                return true;
            case R.id.action_pantry:
                productList.clear();
                getSupportActionBar().setTitle("Pantry");
                new bgWorker().execute("https://www.metro.ca/en/online-grocery/aisles/pantry");
                return true;
            case R.id.action_beverages:
                productList.clear();
                getSupportActionBar().setTitle("Beverages");
                new bgWorker().execute(" https://www.metro.ca/en/online-grocery/aisles/beverages");
                return true;
            case R.id.action_beer:
                productList.clear();
                getSupportActionBar().setTitle("Beer & Wine");
                new bgWorker().execute(" https://www.metro.ca/en/online-grocery/aisles/beer-wine");
                return true;
            case R.id.action_meat:
                productList.clear();
                getSupportActionBar().setTitle("Meat & Poultry");
                new bgWorker().execute(" https://www.metro.ca/en/online-grocery/aisles/meat-poultry");
                return true;
            case R.id.vegan:
                productList.clear();
                getSupportActionBar().setTitle("Vegan & Vegetarian");
                new bgWorker().execute(" https://www.metro.ca/en/online-grocery/aisles/vegan-vegetarian-food");
                return true;
            case R.id.Organic:
                productList.clear();
                getSupportActionBar().setTitle("Organic Groceries");
                new bgWorker().execute(" https://www.metro.ca/en/online-grocery/aisles/organic-groceries");
                return true;
            case R.id.Snacks:
                productList.clear();
                getSupportActionBar().setTitle("Snacks");
                new bgWorker().execute(" https://www.metro.ca/en/online-grocery/aisles/snacks");
                return true;
            case R.id.frozen:
                productList.clear();
                getSupportActionBar().setTitle("Frozen");
                new bgWorker().execute(" https://www.metro.ca/en/online-grocery/aisles/frozen");
                return true;
            case R.id.bread:
                productList.clear();
                getSupportActionBar().setTitle("Bread & Bakery");
                new bgWorker().execute(" https://www.metro.ca/en/online-grocery/aisles/bread-bakery-products");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class bgWorker extends AsyncTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          //  pb.setMax(100);
        }

//        @Override
//        protected void onProgressUpdate(Object[] values) {
//       //     super.onProgressUpdate(values);
//           // pb.setProgress(10);
//        }

        @Override
        protected Object doInBackground(Object[] objects) {
            String img="default";
            String nom="default";
            String prix="default";
            String info="default";

            String url="https://www.metro.ca/en/online-grocery/aisles/fruits-vegetables";

            try {
              //  Document doc= Jsoup.connect("https://www.metro.ca/en/online-grocery/aisles/fruits-vegetables").get();
                Document doc= Jsoup.connect(objects[0].toString()).get();

                for (Element e:doc.select("div.products-tile-list__tile")){
                nom = e.select("div.pt-title").text();
                img = e.select("img").attr("src").toString();
                info = e.select("div.pi--main-price").text();
                prix = e.select("span.pi-price.price-update").text();
                    productList.add((new Produit(nom,img,info,prix)));
                    System.out.println(nom+"\n"+img+"\n"+info+"\n"+prix);
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            simpleList = (GridView) findViewById(R.id.simpleGridView);
            //productList.add(new Produit("Banana","https://product-images.metro.ca/images/hee/h5c/8872794652702.jpg","$0.33 avg. ea. (190 g avg.) $1.74 /kg$0.79 /lb.","$0.33"));
//        productList.add(new Produit("Lapin",R.drawable.raouf));
//        productList.add(new Produit("bitch",R.drawable.raouf));
//        productList.add(new Produit("Sale",R.drawable.raouf));
//        productList.add(new Produit("batard",R.drawable.raouf));
            MyAdapter myAdapter=new MyAdapter(MainActivity.this,R.layout.grid_view_items,productList);
            simpleList.setAdapter(myAdapter);

        }
    }



}
