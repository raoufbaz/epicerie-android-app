package com.example.lab2;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PanierActivity extends AppCompatActivity {
    GridView simpleList;
    ArrayList<Produit> productList= Produit.cart;
    TextView qty;
    TextView nom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panier);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        simpleList = (GridView) findViewById(R.id.simpleGridView);
        CartAdapter myAdapter=new CartAdapter(PanierActivity.this,R.layout.grid_view_cartitems,productList);
        simpleList.setAdapter(myAdapter);

    }
    public void total(android.view.View v){
        double total=0;
        for (Produit p: Produit.cart) {
            total+=p.getPrice()*p.qty;
        }
        Toast.makeText(this, "The total is: " + String.format("%.2f", total)+ " $", Toast.LENGTH_SHORT).show();
    }

}
